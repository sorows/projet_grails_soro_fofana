<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Default | Equation - Multipurpose Bootstrap Dashboard Template </title>
    <link rel="icon" type="image/x-icon" href="../../assets/image/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="../../assets/css/loader.css" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="../../assets/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/plugins/charts/chartist/chartist.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/default-dashboard/style.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->






</head>
<body>

    <div id="eq-loader">
        <div class="eq-loader-div">
            <div class="eq-loading dual-loader mx-auto mb-5"></div>
        </div>
    </div>

    <!--  BEGIN NAVBAR  -->
    <header class="desktop-nav header navbar fixed-top">
        <div class="nav-logo mr-sm-5 ml-sm-4">
            <a href="javascript:void(0);" class="nav-link sidebarCollapse d-inline-block mr-sm-5" data-placement="bottom">
                <i class="flaticon-menu-line-3"></i>
            </a>
            <a href="index.html" class=""> <img src="../../assets/img/1.png" class="img-fluid" alt="logo"></a>
        </div>
        <ul class="navbar-nav flex-row mr-auto">
            <li class="nav-item ml-4 d-lg-none d-sm-block d-none">
                <form class="form-inline search-full form-inline search animated-search" role="search">
                    <i class="flaticon-search-1 d-lg-none d-block"></i>
                    <input type="text" class="form-control search-form-control  ml-lg-auto" placeholder="Search...">
                </form>
            </li>
            <li class="nav-item d-lg-block d-none">
                <form class="form-inline form-inline search animated-search" role="search">
                    <i class="flaticon-search-1 d-lg-none d-block"></i>
                    <input type="text" class="form-control search-form-control" placeholder="Search here...">
                </form>
            </li>
        </ul>

        <ul class="navbar-nav flex-row ml-lg-auto">


            <li class="nav-item dropdown user-profile-dropdown pl-4 pr-lg-0 pr-2 ml-lg-2 mr-lg-4  align-self-center">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user">
                    <div class="user-profile d-lg-block d-none">
                        <img src="../../assets/img/90x90.jpg" alt="admin-profile" class="img-fluid">
                    </div>
                    <i class="flaticon-user-7 d-lg-none d-block"></i>
                </a>
            </li>
        </ul>
    </header>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="ps-overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN MODERN  -->
        <div class="modernSidebar-nav header navbar">
            <div class="">
                <nav id="modernSidebar">
                    <ul class="menu-categories pl-0 m-0" id="topAccordion">
                        <li class="menu">
                            <a href="#dashboard" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                                <div class="">
                                    <i class="flaticon-computer-6"></i>
                                    <span>Dashboard</span>
                                </div>
                            </a>
                            <div class="collapse submenu list-unstyled eq-animated eq-fadeInUp" id="dashboard" data-parent="#topAccordion">
                                <ul class="submenu-scroll">
                                    <li>
                                        <ul class="list-unstyled sub-submenu" id="dashboards">
                                            <li class="active">
                                                <a href="/#"> <div><i class="flaticon-computer-4"></i> Accueil</div> </a>
                                            </li>


                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="menu">
                            <a href="#uiAndComponents" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
                                <div class="">
                                    <i class="flaticon-3d-cube"></i>
                                    <span>UTILISATEURS</span>
                                </div>
                            </a>
                            <div class="submenu list-unstyled collapse eq-animated eq-fadeInUp" id="uiAndComponents" data-parent="#topAccordion">
                                <div class="submenu-scroll">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#ui-features" data-toggle="collapse" aria-expanded="true" class="i"> <span class="">Utilisateur</span> </a>
                                            <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="ui-features">
                                                <li>
                                                    <a href="/user/index"> Liste des utilisateurs</a>
                                                </li>
                                                <li>
                                                    <a href="/user/create">Créer un utilisateur</a>
                                                </li>






                                    </ul>


                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li class="menu">
                            <a href="#tables-forms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
                                <div class="">
                                    <i class="flaticon-table"></i>
                                    <span>ILLUSTRATIONS</span>
                                </div>
                            </a>
                            <div class="submenu list-unstyled collapse eq-animated eq-fadeInUp" id="tables-forms" data-parent="#topAccordion">
                                <div class="submenu-scroll">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#tables" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">Illustrations</span> </a>
                                            <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="tables">
                                                <li>
                                                    <a href="/illustration/index"> Liste des illustrations </a>
                                                </li>
                                                <li>
                                                    <a href="/illustration/create">Créer une illustration</a>
                                                </li>



                                            </ul>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </li>

                        <li class="menu">
                            <a href="#pages" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
                                <div class="">
                                    <i class="flaticon-copy"></i>
                                    <span>ANNONCES</span>
                                </div>
                            </a>
                            <div class="submenu list-unstyled collapse eq-animated eq-fadeInUp" id="pages" data-parent="#topAccordion">
                                <div class="submenu-scroll">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#ecommerce" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">Annonces</span> </a>
                                            <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="ecommerce">
                                                <li>
                                                    <a href="/Annonce/index ">liste des annonces </a>
                                                </li>
                                                <li>
                                                    <a href="/annonce/create">Créer une annonce</a>
                                                </li>



                                            </ul>
                                        </li>
                                    </ul>


                                </div>
                            </div>
                        </li>





                    </ul>
                </nav>
            </div>
        </div>
        <!--  END MODERN  -->

        <!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="container">
                <div class="page-header">
                    <div class="page-title">
                         <li class="active">
                                                <a href="/#"> <div> <h3>Accueil</h3></div> </a>
                          </li>
                    </div>
                </div>

    <g:layoutBody/>



    </div>
    <!--  END CONTENT PART  -->

</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
    <footer class="footer-section theme-footer">

        <div class="footer-section-1  sidebar-theme">

        </div>

        <div class="footer-section-2 container-fluid">
            <div class="row">
                <div id="toggle-grid" class="col-xl-7 col-md-6 col-sm-6 col-12 text-sm-left text-center">
                    <ul class="list-inline links ml-sm-5">
                        <li class="list-inline-item">
                            <a target="_blank" href="https://themeforest.net/item/equation-responsive-admin-dashboard-template/23191987"></a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-5 col-md-6 col-sm-6 col-12">
                    <ul class="list-inline mb-0 d-flex justify-content-sm-end justify-content-center mr-sm-3 ml-sm-0 mx-3">
                        <li class="list-inline-item  mr-3">
                            <p class="bottom-footer"><a target="_blank" href="https://designreset.com/equation">Soro & Fofana</a></p>
                        </li>
                        <li class="list-inline-item align-self-center">
                            <div class="scrollTop"><i class="flaticon-up-arrow-fill-1"></i></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--  END FOOTER  -->

    <!--  BEGIN PROFILE SIDEBAR  -->
    <aside class="profile-sidebar text-center">
        <div class="profile-content profile-content-scroll">
            <div class="usr-profile">
                <img src="../../assets/images/90x90.jpg" alt="admin-profile" class="img-fluid">
            </div>


            <p class="user-name mt-4 mb-4"><sec:username/></p>
            <div class="">
                <div class="accordion" id="user-stats">
                    <div class="card">
                        <div class="card-header pb-4 mb-4" id="status">
                            <h6 class="mb-0" data-toggle="collapse" data-target="#user-status" aria-expanded="true" aria-controls="user-status"><i class="flaticon-view-3 mr-2"></i> Status <i class="flaticon-down-arrow ml-2"></i> </h6>
                        </div>
                        <div id="user-status" class="collapse show" aria-labelledby="status" data-parent="#user-stats">
                            <div class="card-body text-left">
                                <ul class="list-unstyled pb-4">
                                    <li class="status-online"><a href="javascript:void(0);">Online</a></li>


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-links text-left">
                <ul class="list-unstyled">
                    <li><a href="apps_mailbox.html"><i class="flaticon-mail-22"></i> Inbox</a></li>
                    <li><a href=""><i class="flaticon-user-11"></i> Profil</a></li>
                    <li><a href="/logout"><i class="flaticon-power-off"></i>Deconnexion</a></li>
                </ul>
            </div>
        </div>
    </aside>
    <!--  BEGIN PROFILE SIDEBAR  -->



<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="../../assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="../../assets/js/loader.js"></script>
<script src="../../assets/bootstrap/js/popper.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="../../assets/plugins/blockui/jquery.blockUI.min.js"></script>
<script src="../../assets/js/app.js"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="../../assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="../../assets/plugins/charts/chartist/chartist.js"></script>
<script src="../../assets/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js"></script>
<script src="../../assets/plugins/maps/vector/jvector/worldmap_script/jquery-jvectormap-world-mill-en.js"></script>
<script src="../../assets/plugins/calendar/pignose/moment.latest.min.js"></script>
<script src="../../assets/plugins/calendar/pignose/pignose.calendar.js"></script>
<script src="../../assets/plugins/progressbar/progressbar.min.js"></script>
<script src="../../assets/js/default-dashboard/default-custom.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->


</body>
</html>