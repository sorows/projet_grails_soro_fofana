<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />


    </head>
    <body>

    <div class="row"  id="create-user" role="main">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow" background-color="#004B8C">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Inscription des utlisateurs </h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area add-manage-product-2">
                    <div class="row">
                        <div class="col-xl-7 col-md-12">
                            <div class="card card-default">
                                <div class="card-heading"><h2 class="card-title"><span>Inscription</span></h2></div>
                                <div class="card-body">
                                    <div class="card-body">
                                      <g:form resource="${this.user}" class="form-horizontal"  method="POST">

                                            <div class="form-group mb-4">
                                                <div class="row">
                                                    <label class="col-md-4">Nom:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" name="username" value="" required="" id="username"  type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <div class="row">
                                                    <label class="col-md-4">Mot de passe:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="password" name="password" required="" value="" id="password"  required=""  >
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="form-group mb-4">
                                                <div class="row">
                                                    <label class="col-md-4">Role :</label>
                                                    <div class="col-md-8">
                                                        <g:select class="form-control form-custom mb-4" name="role" from="${roles}" optionKey="id" optionValue="authority"/>

                                                    </div>
                                                </div>
                                            </div>







                                          <div class="row">
                                              <div class="col-12 text-center  pb-4">
                                                  <button  name="create" class="save"
                                                           value="${message(code: 'default.button.create.label', default: 'Create') }">Enregistrer</button>
                                              </div>
                                          </div>

                                      </g:form>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>



