<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-user" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.user}" method="PUT">
                <g:hiddenField name="version" value="${this.user?.version}" />
                <fieldset class="form">
                    <f:all bean="user"/>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />

</head>
<body>

<div class="row"  id="create-user" role="main">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow" background-color="#004B8C">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Modifier l'utlisateur </h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area add-manage-product-2">
                <div class="row">
                    <div class="col-xl-7 col-md-12">
                        <div class="card card-default">
                            <div class="card-heading"><h2 class="card-title"><span>Modification</span></h2></div>
                            <div class="card-body">
                                <div class="card-body">
                                    <g:form resource="${this.user}" class="form-horizontal"  method="PUT">

                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Nom:</label>
                                                <div class="col-md-8">
                                                    <f:field class="form-control" bean="user" property="username" label=" " />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Mot de passe:</label>
                                                <div class="col-md-8">
                                                    <f:field class="form-control" bean="user" property="password" label=" " />
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Role :</label>
                                                <div class="col-md-8">
                                                    <g:select class="form-control form-custom mb-4" name="role" from="${projet.grails.mbds.esatic.Role.getAll()}" optionKey="authority" optionValue="authority"/>

                                                </div>
                                            </div>
                                        </div>







                                        <div class="row">
                                            <div class="col-12 text-center  pb-4">
                                                <button  name="create" class="save"
                                                         value="${message(code: 'default.button.create.label', default: 'Create') }">Enregistrer</button>
                                            </div>
                                        </div>

                                    </g:form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



