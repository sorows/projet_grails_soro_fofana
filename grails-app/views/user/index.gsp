
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

</head>
<body>



<div class="col-xl-15 col-lg-30 col-md-40 col-40 layout-spacing">

    <div class="row">
<g:each in="${userList}" var="user">

        <div class="col-sm-4 col-12 mb-sm-0 mb-4">
            <div class="widget-content-area social-likes text-center p-0  br-4">
                <div class="card facebook">
                    <div class="icon mb-4">
                        <i class="flaticon-facebook-logo"></i>
                    </div>
                    <div class="card-content">
                        <h5>${user.username}</h5>
                        
                    </div>
                    <div class="card-btn-section">
                        <p><g:link controller="user" action="show" id="${user.id}">voir details</g:link>
                        </p>
                    </div>
                </div>
            </div>
            <br>




        </div>
</g:each>
    </div>




    </div>
    <!--  END CONTENT PART  -->
</body>
</html>

