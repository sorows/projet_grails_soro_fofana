<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

</head>
<body>


<div class="row layout-spacing ">

    <div class="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
        <div class="widget-content-area  data-widgets br-4">
            <div class="widget  t-sales-widget">
                <div class="media">
                    <div class="icon ml-2">
                        <i class="flaticon-line-chart"></i>
                    </div>
                    <div class="media-body text-right">
                        <p class="widget-text mb-0">Utilisateurs</p>
                        <p class="widget-numeric-value">Nombre:${projet.grails.mbds.esatic.User.count()}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
        <div class="widget-content-area  data-widgets br-4">
            <div class="widget  t-order-widget">
                <div class="media">
                    <div class="icon ml-2">
                        <i class="flaticon-cart-bag"></i>
                    </div>
                    <div class="media-body text-right">
                        <p class="widget-text mb-0">Annonces</p>
                        <p class="widget-numeric-value">Nombre:${projet.grails.mbds.esatic.Annonce.count()}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 mb-sm-0 mb-4">
        <div class="widget-content-area  data-widgets br-4">
            <div class="widget  t-customer-widget">
                <div class="media">
                    <div class="icon ml-2">
                        <i class="flaticon-user-11"></i>
                    </div>
                    <div class="media-body text-right">
                        <p class="widget-text mb-0">illustrations</p>
                        <p class="widget-numeric-value">${projet.grails.mbds.esatic.Illustration.count()}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>









<div class="row">
    <div class="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area event-calendar p-0  h-100 br-4">
            <div class="calendar"></div>
        </div>
    </div>

    <div class="col-xl-6 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="top-searches widget-content-area p-0 widget-content-container h-100 br-4">
            <div class="col-lg-12 col-md-12 col-12 map-title">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-6">
                        <p class="mb-4 mt-2">Pays</p>
                    </div>
                </div>
            </div>
            <div class="child-content">
                <div id="world-map" style="height: 435px; max-height: 100%;"></div>
                <div class="world-map-section">
                    <div class="">
                        <div class="table-responsive top-search-scroll">
                            <table class="table table-highlight-head">
                                <thead>
                                <tr>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-1 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">USA</span>
                                        </div>
                                    </th>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-2 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">Australia</span>
                                        </div>
                                    </th>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-3 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">Spain</span>
                                        </div>
                                    </th>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-4 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">France</span>
                                        </div>
                                    </th>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-5 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">India</span>
                                        </div>
                                    </th>
                                    <th class="align-center">
                                        <div class="d-flex justify-content-center">
                                            <div class="d-m-data-6 data-marker align-self-center"></div>
                                            <span class="page-view mr-sm-3">Other</span>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="align-center data-value-1">55%</td>
                                    <td class="align-center data-value-2">30%</td>
                                    <td class="align-center data-value-3">10%</td>
                                    <td class="align-center data-value-4">4%</td>
                                    <td class="align-center data-value-5">0.6%</td>
                                    <td class="align-center data-value-6">0.4%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xl-5 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area page-views p-0  br-4">
            <ul class="nav nav-pills py-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Utilisteurs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Illustrations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Annonces</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 text-center">
                            <div class="daily">
                                <p class="d-count mb-0">${projet.grails.mbds.esatic.User.count()} </p>
                                <p>Personnes</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div id="daily"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 text-center">
                            <div class="weekly">
                                <p class="w-count mb-0">${projet.grails.mbds.esatic.Illustration.count()}</p>
                                <p>Illustrations</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div id="weekly"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 text-center">
                            <div class="month">
                                <p class="m-count mb-0">${projet.grails.mbds.esatic.Annonce.count()}</p>
                                <p>Annonces</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div id="month"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div>

</body>
</html>
