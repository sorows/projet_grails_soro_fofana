
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />

</head>
<body>

<div class="row"  id="create-user" role="main">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow" background-color="#004B8C">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Inscription des illustrations </h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area add-manage-product-2">
                <div class="row">
                    <div class="col-xl-7 col-md-12">
                        <div class="card card-default">
                            <div class="card-heading"><h2 class="card-title"><span>Inscription</span></h2></div>
                            <div class="card-body">
                                <div class="card-body">
                                    <g:form resource="${this.illustration}" class="form-horizontal"  method="POST">


                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Filename:</label>
                                                <div class="widget-content widget-content-area">




                                                        <f:field bean="illustration" property="filename" label=" "/>






                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Annonces:</label>

                                                <div class="col-md-8">
                                                    <f:field bean="illustration" property="annonce" label=" "/>
                                                </div>
                                            </div>
                                        </div>







                                        <div class="row">
                                            <div class="col-12 text-center  pb-4">
                                                <button  name="create" class="save"
                                                         value="${message(code: 'default.button.create.label', default: 'Create') }">Enregistrer</button>
                                            </div>
                                        </div>

                                    </g:form>

                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



