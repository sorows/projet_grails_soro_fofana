<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'illustration.label', default: 'Illustration')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <ul class="navbar-nav flex-row mr-auto">

            <li class="nav-item d-lg-block d-none">
                <form class="form-inline form-inline search animated-search" role="search">
                    <i class="flaticon-search-1 d-lg-none d-block"></i>
                    <input type="text" class="form-control search-form-control" placeholder="Search here...">
                </form>
            </li>
        </ul>
    </head>
    <body>

    <div class="row layout-spacing">
        <div class="col-lg-12">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Liste</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive mb-4">
                        <table id="customer-info-detail-3" class="table style-3 table-bordered  table-hover">
                            <thead>
                            <tr>
                                <th class="checkbox-column text-center"> Numéro </th>
                                <th class="align-center">Filename</th>
                                <th>Annonce</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                           <g:each in="${illustrationList}" var="illustration">
                            <tr>
                                <td class="checkbox-column text-center">${illustration.id} </td>
                                <td class="align-center">
                                    <span>
                                        <img src="${grailsApplication.config.assets.url + illustration.filename}"/>
                                    </span>
                                </td>

                                <td class="align-center"><span class="shadow-none badge badge-success">
                                    ${illustration.annonce.description}
                                </span></td>
                                <td class="text-center">
                                    <ul class="table-controls">
                                        <g:link data-original-title="Modifier" controller="illustration" id="${illustration.id}" class="flaticon-edit bs-tooltip" action="edit" resource="${this.illustration}"></g:link>
                                        <g:form resource="${this.illustration}" method="DELETE">
                                            <input class="flaticon-delete  p-1 br-6 mb-1" type="submit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"

                                                   value="${message(code: 'default.button.delete.label', default: 'Supprimer')}"
                                                   onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Vous etes sure?')}');"/>

                                        </g:form>

                                    </ul>
                                </td>
                            </tr>



                          </g:each>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="../assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="../assets/js/loader.js"></script>
    <script src="../assets/bootstrap/js/popper.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../assets/js/app.js"></script>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="../assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/plugins/table/datatable/datatables.js"></script>
    <script>
        // var e;
        c1 = $('#customer-info-detail-1').DataTable({
            "lengthMenu": [ 5, 10, 20, 50, 100 ],
            headerCallback:function(e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML='<label class="new-control new-checkbox checkbox-outline-primary m-auto">\n<input type="checkbox" class="new-control-input chk-parent select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
            },
            columnDefs:[ {
                targets:0, width:"30px", className:"", orderable:!1, render:function(e, a, t, n) {
                    return'<label class="new-control new-checkbox checkbox-outline-primary  m-auto">\n<input type="checkbox" class="new-control-input child-chk select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
                }
            }],
            "language": {
                "paginate": {
                    "previous": "<i class='flaticon-arrow-left-1'></i>",
                    "next": "<i class='flaticon-arrow-right'></i>"
                },
                "info": "Showing page _PAGE_ of _PAGES_"
            }
        });

        multiCheck(c1);

        c2 = $('#customer-info-detail-2').DataTable({
            "lengthMenu": [ 5, 10, 20, 50, 100 ],
            headerCallback:function(e, a, t, n, s) {
                e.getElementsByTagName("th")[0].innerHTML='<label class="new-control new-checkbox checkbox-outline-primary m-auto">\n<input type="checkbox" class="new-control-input chk-parent select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
            },
            columnDefs:[ {
                targets:0, width:"30px", className:"", orderable:!1, render:function(e, a, t, n) {
                    return'<label class="new-control new-checkbox checkbox-outline-primary  m-auto">\n<input type="checkbox" class="new-control-input child-chk select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
                }
            }],
            "language": {
                "paginate": {
                    "previous": "<i class='flaticon-arrow-left-1'></i>",
                    "next": "<i class='flaticon-arrow-right'></i>"
                },
                "info": "Showing page _PAGE_ of _PAGES_"
            }
        });

        multiCheck(c2);

        c3 = $('#customer-info-detail-3').DataTable({
            "lengthMenu": [ 5, 10, 20, 50, 100 ],
            "language": {
                "paginate": {
                    "previous": "<i class='flaticon-arrow-left-1'></i>",
                    "next": "<i class='flaticon-arrow-right'></i>"
                },
                "info": "Showing page _PAGE_ of _PAGES_"
            }
        });

        multiCheck(c3);
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->

    </body>
</html>