
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Register-3 | Equation - Multipurpose Bootstrap Dashboard Template </title>
    <link rel="icon" type="image/x-icon" href="../assets/images/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/users/register-3.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

</head>
<body class="login">
<form class="form-login"  action="${postUrl ?: '/login/authenticate'}" method="POST"  autocomplete="off">
    <div class="row">
        <div class="col-md-12 text-center mb-4">
            <img alt="logo" src="../assets/images/logo-6.png" class="theme-logo">
        </div>
        <div class="col-md-12">
            <label for="inputUsername" class="">USERNAME</label>
            <input type="text"  name="${usernameParameter ?: 'username'}" id="username" class="form-control mb-4" placeholder="Login" required >
            <label for="inputPassword" class="">PASSWORD</label>
            <input type="password"  name="${passwordParameter ?: 'password'}" id="password" class="form-control mb-4" placeholder="Password" required>
            <div class="terms-conditions-chkbox d-flex justify-content-center mb-4">
                <div class="custom-control custom-checkbox mr-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>value="remember-me">
                    <label class="custom-control-label" for="customCheck1"><span class="d-block mt-1">Rester connecté <a href="">Terms</a></span></label>
                </div>
            </div>
            <button type="submit" class="btn btn-gradient-dark btn-rounded btn-block">Valider</button>

            <div class="forgot-pass text-center mt-4">
                <a href="user_login_3.html">Back</a>
            </div>
        </div>
    </div>
</form>

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="../assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="../assets/bootstrap/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>
