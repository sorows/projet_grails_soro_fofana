
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />

</head>
<body>
<div class="row">
    <div class="btn-group  mb-4 mr-2">
        <button type="button" class="btn btn-success br-left-30">Action</button>
        <button type="button" class="btn btn-success br-right-30 dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu">

            <g:link class="dropdown-item" action="edit" resource="${this.annonce}"><g:message code="default.button.edit.label" default="modifier" /></g:link>

            <g:form resource="${this.annonce}" method="DELETE">
                <fieldset class="buttons">

                    <input class="dropdown-item" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
            <a class="dropdown-item" href="index">page d'acceuil</a>

        </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Information de l'annonce</h4>
                    </div>
                </div>
            </div>

            <div class="widget-content widget-content-area add-sub-category">
                <div class="row">
                    <div class="mx-xl-auto col-xl-10 col-md-12">
                        <div class="card card-default">
                            <div class="card-heading"><h2 class="card-title"><span>Annonce</span></h2></div>
                            <div class="card-body">
                                <div class="card-body">

                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Title :</label>
                                            <div class="col-md-8">
                                                ${annonce.title}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Description:</label>
                                            <div class="col-md-8">
                                                ${annonce.description}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Price</label>
                                            <div class="col-md-8">
                                                ${annonce.price}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Status</label>
                                            <div class="col-md-8">
                                                ${annonce.status}


                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Illustration</label>
                                            <div class="col-md-8">

                                                <g:each in="${annonce.illustrations}" var="illustration">
                                                    <img src="${grailsApplication.config.assets.url + illustration.filename}"/>
                                                </g:each>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4">Author :</label>
                                            <div class="col-md-8">
                                                <g:link controller="user" action="show" id="${annonce.author.id}">
                                                    ${annonce.author.username}
                                                </g:link>

                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
