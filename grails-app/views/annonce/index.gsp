<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>

<div class="row">

    <div class="col-xl-12 col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow driver-list br-4">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Liste des annonces</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area p-0">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="form-check-column text-center">
                                <label for="driverListAll" class="new-control new-checkbox checkbox-primary pb-2">
                                    <input type="checkbox" id="driverListAll" class="new-control-input">

                                </label>
                            </th>
                            <th>Title</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Price Status</th>
                            <th class="text-center">Illustrations</th>
                            <th class="text-center">Price</th>

                            <th class="text-center">Author</th>
                            <th class="text-center">Actions</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${annonceList}" var="annonce">













                            <tr>
                                <td class="form-check-column text-center">
                                    <label class="new-control new-checkbox checkbox-primary pb-2">

                                    </label>
                                </td>
                                <td>

                                    <div class="d-inline-block">
                                        <span class="">


                                        </span>
                                        <div class=" mt-1 d-inline-block">
                                            <h6 class="mt-2 mb-0">${annonce.title}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">${annonce.description}</td>
                                <td class="text-center"><div class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Available">${annonce.status}</div></td>
                                <td class="align-center"><g:each in="${annonce.illustrations}" var="illustration">
                                    <img src="${grailsApplication.config.assets.url + illustration.filename}"/>
                                </g:each>
                                </td>
                                <td class="align-center">${annonce.price}</td>
                                <td class="text-center">
                                    <span></span>
                                    <div class="t-d-rating">
                                        <i class=""><g:link controller="user" action="show" id="${annonce.author.id}">
                                            ${annonce.author.username}
                                        </g:link></i>

                                    </div>
                                </td>
                                <td>
                                    <g:link data-original-title="Modifier" controller="annonce" id="${annonce.id}" class="flaticon-edit bs-tooltip" action="edit" resource="${this.annonce}"></g:link>
                                    <g:form resource="${this.annonce}" method="DELETE">
                                        <input class="flaticon-delete  p-1 br-6 mb-1" type="submit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer"

                                               value="${message(code: 'default.button.delete.label', default: 'Supprimer')}"
                                               onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Vous etes sure?')}');"/>

                                    </g:form>

                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>