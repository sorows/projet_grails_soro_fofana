

<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
<g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
<title><g:message code="default.create.label" args="[entityName]" /></title>
<link rel="icon" type="image/x-icon" href="../assets/img/favicon.ico"/>
<link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<!-- END GLOBAL MANDATORY STYLES -->

<!--  BEGIN CUSTOM STYLE FILE  -->
<link href="../assets/css/ecommerce/addedit_categories.css" rel="stylesheet" type="text/css">

<link rel="icon" type="image/x-icon" href="../assets/img/favicon.ico"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../assets/plugins/mdl/material.css">
</head>


<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Ajout d'une annonce</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area add-category">
                <div class="row">
                    <div class="mx-xl-auto col-xl-10 col-md-12">
                        <div class="card card-default">
                            <div class="card-heading"><h2 class="card-title"><span>Annonce</span></h2></div>

                            <div class="card-body">
                                <div class="card-body">
                                    <form action="/annonce/save" method="post" >


                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Title :</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" value="" required="" id="title" name="title" type="text">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Description :</label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" rows="5" cols="5" id="desciption" value="" name="description"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Price :</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="number decimal" id="price"  required="" value="" id="price" name="price" type="text">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xl-4 col-lg-6 col-sm-6  layout-spacing">
                                        <div class="statbox widget box box-shadow">

                                            <div class="widget-content widget-content-area">
                                                <div class="mb-4">

                                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                                                        <input type="hidden"  name="_status" />
                                                        <input type="checkbox"name="status" id="checkbox-1" class="mdl-checkbox__input" checked>
                                                        <span class="mdl-checkbox__label">Status</span>
                                                    </label>

                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="form-group mb-4">
                                            <div class="row">
                                                <label class="col-md-4">Author :</label>

                                                <div class="col-md-8">
                                                     <g:select class="form-control form-custom mb-4" name="author" from="${projet.grails.mbds.esatic.User.getAll()}" optionKey="id" optionValue="username"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for='illustrations'>Illustrations</label><ul></ul><a href="/illustration/create?annonce.id=">Ajouter une  Illustration</a>
                                        </div>
                                 <fieldset class="buttons">
                                     <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                                 </fieldset>
                                    </form>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="../assets/js/loader.js"></script>
<script src="../assets/bootstrap/js/popper.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="../assets/plugins/blockui/jquery.blockUI.min.js"></script>
<script src="../assets/js/app.js"></script>

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="../assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/plugins/mdl/material.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

</body>
</html>